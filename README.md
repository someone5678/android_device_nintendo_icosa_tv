Device configuration for the Nintendo Switch with SwitchRoot mods
==============================================================

#### Copyright (C) 2021 LineageOS

The Nintendo Switch (**icosa_tv**) is a video game console from Nintendo.

## Device specifications

Component   | Description
-------:|:-------------------------
Chipset | NVIDIA Tegra X1 T210
CPU     | 4 Cores: 4x1.02 GHz Cortex-A57
GPU     | 256 Maxwell-based CUDA cores
Memory  | 4 GB RAM
Storage | 32 GB (eMMC)
Battery | Non-removable Li-ion 4310 mAh
Display | 1280 x 720 pixels, 6.20 inches (~237 ppi pixel density)
SD card slot | Dedicated microSDXC slot
